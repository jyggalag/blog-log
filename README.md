# blog-log

Simple blog with private and public log created in Django 1.11 and Python 3.6.

<hr />
<p>
    Web application containing two smaller apps: Log and Blog<br />
    Cannot register users. The superuser is already in database and can log in via personal link which is known only by him.
</p>
<h2><a href="#log_app">Log application:</a></h2>
<p>
    Users (visitors) can only see public topics and included entries.<br />
    Superuser can also add new topics and entries, edit and delete them. He can mark them as public or private. Private topics are visible only for logged user (superuser)
</p>
<h2><a href="#blog_app">Blog application:</a></h2>
<p>
    User can see posts and entries. <br />
    Superuser can add new posts, edit and delete them.
</p>

<hr>
