from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import logout


def logout_view(request):
    """ user logourt """
    logout(request)
    return HttpResponseRedirect(reverse('blog_app:index'))


