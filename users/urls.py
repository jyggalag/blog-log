""" defines patterns for users app """

from django.conf.urls import url
from django.contrib.auth.views import login

from . import views

urlpatterns = [
    # login site
    url(r'^log_me_pls/$', login, {'template_name': 'users/login.html'}, name='login'),
    # logout
    url(r'^logout_me_pls/$', views.logout_view, name='logout'),
]