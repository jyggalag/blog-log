from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from .models import Topic, Entry
from .forms import TopicForm, EntryForm

def log_index(request):
    """ main page for log_app """
    return render(request, 'log_app/log_index.html')

def topics(request):
    """ show topics """
    topics = Topic.objects.order_by('date_added')
    context = {'topics': topics}
    return render(request, 'log_app/topics.html', context)

def topic(request, topic_name):
    """ shows single topic and his entries """
    topic = Topic.objects.get(text=topic_name)
    entries = topic.entry_set.order_by('-date_added')
    context = {'topic': topic, 'entries': entries}
    return render(request, 'log_app/topic.html', context)

@login_required
def new_topic(request):
    """ add new topic """
    if request.method != 'POST':
        # no data transfered
        form = TopicForm()
    else:
        # transfered data as POST request
        form = TopicForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('log_app:topics'))
        
    context = {'form': form}
    return render(request, 'log_app/new_topic.html', context)

@login_required
def new_entry(request, topic_name):
    """ add new entry """
    topic = Topic.objects.get(text=topic_name)

    if request.method != 'POST':
        # no data
        form = EntryForm()
    else:
        # data transfered as post
        form = EntryForm(data=request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.topic = topic
            new_entry.save()
            return HttpResponseRedirect(reverse('log_app:topic', args=[topic_name]))

    context = {'topic': topic, 'form': form}
    return render(request, 'log_app/new_entry.html', context)

@login_required
def edit_entry(request, entry_id):
    """ edit entry """
    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic

    if request.method != 'POST':
        # no data
        form = EntryForm(instance=entry)
    else:
        # data as POST
        form = EntryForm(instance=entry, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('log_app:topic', args=[topic.text]))

    context = {'entry': entry, 'topic': topic, 'form': form}
    return render(request, 'log_app/edit_entry.html', context)

@login_required
def delete_entry(request, entry_id):
    """ delete entry """
    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic
    entry.delete()
    return HttpResponseRedirect(reverse('log_app:topic', args=[topic.text]))

@login_required
def delete_topic(request, topic_name):
    """ delete topic """
    topic = Topic.objects.get(text=topic_name)
    topic.delete()
    return HttpResponseRedirect(reverse('log_app:topics'))


