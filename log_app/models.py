from django.db import models

class Topic(models.Model):
    """Topic learning by user"""
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    private = models.BooleanField(default=False)

    def __str__(self):
        """returns model representation as a string"""
        return self.text


class Entry(models.Model):
    """learning progress"""
    topic = models.ForeignKey(Topic)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'entries'

    def __str__(self):
        """return strin representation"""
        return self.text[:50] + "..."

