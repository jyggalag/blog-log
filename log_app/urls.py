""" url patterns for log_app """
from django.conf.urls import url

from . import views

urlpatterns = [
    # main site
    url(r'^log_app/$', views.log_index, name='log_index'),
    # topics
    url(r'^log_app/topics/$', views.topics, name='topics'),
    # single topic
    url(r'^log_app/topics/(?P<topic_name>\w+)/$', views.topic, name='topic'),
    # add topic
    url(r'^log_app/new_topic/$', views.new_topic, name='new_topic'),
    # new entry
    url(r'^log_app/new_entry/(?P<topic_name>\w+)/$', views.new_entry, name='new_entry'),
    # edit entry
    url(r'^logg_app/edit_entry/(?P<entry_id>\d+)/$', views.edit_entry, name='edit_entry'),
    # delete entry
    url(r'^logg_app/delete_entry/(?P<entry_id>\d+)/$', views.delete_entry, name='delete_entry'),
    #delete topic
    url(r'^logg_app/delete_topic/(?P<topic_name>\w+)/$', views.delete_topic, name='delete_topic'),
]