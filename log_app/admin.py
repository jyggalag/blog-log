from django.contrib import admin

from log_app.models import Topic, Entry
from blog_app.models import BlogPost, Category, Comment

admin.site.register(Topic)
admin.site.register(Entry)
admin.site.register(Category)
admin.site.register(BlogPost)
admin.site.register(Comment)
