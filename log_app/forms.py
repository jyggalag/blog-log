from django import forms

from .models import Topic, Entry

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ['text','private']
        labels = {'text': '', 'private': 'prywatny?'}
        widgets = {'private': forms.CheckboxInput()}

class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ['text']
        labels = {'text': '', 'private': 'prywatny?'}
        widgets = {'text': forms.Textarea(attrs={'cols': 80})}