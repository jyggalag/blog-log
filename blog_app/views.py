from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import BlogPost, Category, Comment
from .forms import CommentForm, NewPostForm, AddCategoryForm

def index(request):
    """ render main site """
    return render(request, 'blog_app/index.html')

def blog_index(request):
    """ show blog posts """
    blog_posts = BlogPost.objects.order_by('-date_added')
    categories = Category.objects.all()
    paginator = Paginator(blog_posts, 5)
    page = request.GET.get('page')
    try:
        blog_posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        blog_posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        blog_posts = paginator.page(paginator.num_pages)
    context = {'blog_posts': blog_posts, 'categories': categories}
    return render(request, 'blog_app/blog_index.html', context)

def category(request, category_name):
    """ show posts from category """
    categories = Category.objects.all()
    category = Category.objects.get(text=category_name)
    blog_posts = BlogPost.objects.filter(category__text=category_name).order_by('-date_added')
    context = {'category': category, 'blog_posts': blog_posts, 'categories': categories}
    return render(request, 'blog_app/category.html', context)
 
def show_post(request, post_id):
    """ show single post """
    categories = Category.objects.all()
    blog_post = BlogPost.objects.get(id=post_id)

    # add topic form """
    if request.method != 'POST':
        # no data transfered
        form = CommentForm()
    else:
        # data transfered as POST
        form = CommentForm(request.POST)
        if form.is_valid():
            data = request.POST.get("checker")
            if data == "6":
                new_comment = form.save(commit=False)
                new_comment.post = blog_post
                new_comment.save()
                return HttpResponseRedirect(reverse('blog_app:show_post', args=[post_id]))
            else:
                return HttpResponseRedirect(reverse('blog_app:show_post', args=[post_id]))
    
    # show comments
    try:
        comments = Comment.objects.filter(post__id=post_id).order_by('-date_added')
    except:
        comments = {}
    context = {'blog_post': blog_post, 'categories': categories, 'comments': comments, 'form': form}
    return render(request, 'blog_app/show_post.html', context)

@login_required
def delete_comment(request,comment_id):
    """ delete comment """
    comment = Comment.objects.get(id=comment_id)
    comment.delete()
    return HttpResponseRedirect(reverse('blog_app:show_post', args=[comment.post.id]))


@login_required
def new_post(request):
    """ create new post """
    categories = Category.objects.all()

    # new post form
    if request.method != 'POST':
        # no data transfered
        form = NewPostForm()
    else:
        # data transfered as POST
        form = NewPostForm(request.POST)
        form.save()
        return HttpResponseRedirect(reverse('blog_app:blog_index'))

    context = {'form': form, 'categories': categories}
    return render(request, 'blog_app/new_post.html', context)

@login_required
def edit_post(request, post_id):
    """ edit post """
    blog_post = BlogPost.objects.get(id=post_id)

    if request.method != 'POST':
        # no data
        form = NewPostForm(instance=blog_post)
    else:
        # data as POST
        form = NewPostForm(instance=blog_post, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('blog_app:show_post', args=[blog_post.id]))

    context = {'blog_post': blog_post, 'form': form}
    return render(request, 'blog_app/edit_post.html', context)

@login_required
def delete_post(request, post_id):
    """ deletes post """
    blog_post = BlogPost.objects.get(id=post_id)
    blog_post.delete()
    return HttpResponseRedirect(reverse('blog_app:blog_index'))

@login_required
def add_category(request):
    """ adds new category """
    if request.method != 'POST':
        # no data transfered
        form = AddCategoryForm()
    else:
        # data transfered to POST
        form = AddCategoryForm(request.POST)
        form.save()
        return HttpResponseRedirect(reverse('blog_app:blog_index'))
    
    context = {'form': form}
    return render(request, 'blog_app/add_category.html', context)


    

