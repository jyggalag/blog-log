""" url patterns for log_app """
from django.conf.urls import url

from . import views

urlpatterns = [
    # main site
    url(r'^$', views.index, name='index'),
    # blog site
    url(r'^blog_app/$', views.blog_index, name='blog_index'),
    # posts in one category
    url(r'^blog_app/category/(?P<category_name>\w+)/$', views.category, name='category'),
    # show post
    url(r'^blog_app/post_(?P<post_id>\d+)/$', views.show_post, name='show_post'),
    # add new post
    url(r'^blog_app/new_post/$', views.new_post, name='new_post'),
    # add new category
    url(r'^blog_app/new_category/$', views.add_category, name='add_category'),
    # edit post
    url(r'^blog_app/edit_post/post_(?P<post_id>\d+)/$', views.edit_post, name='edit_post'),
    # delete post
    url(r'^blog_app/delete_post/post(?P<post_id>\d+)/$', views.delete_post, name='delete_post'),
    # delete comment
    url(r'^blog_app/del_comment/comment_(?P<comment_id>\d+)/$', views.delete_comment, name='delete_comment'),
]