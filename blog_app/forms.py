from django import forms

from .models import Category, BlogPost, Comment


class CommentForm(forms.ModelForm):
    """ form for comments """
    class Meta:
        model = Comment
        fields = ['author', 'text']
        labels = {'author': 'nazwa', 'text': 'treść'}
        widgets = {'text': forms.Textarea(attrs={'cols': 80, 'rows': 3})}


class NewPostForm(forms.ModelForm):
    """ form for new post """
    class Meta:
        model = BlogPost
        fields = ['category','title', 'post']
        labels = {'category': 'kategoria', 'title': 'Tytuł', 'post': ''}
        widgets = {'post': forms.Textarea(attrs={'id': 'summernote', 'cols': 100, 'rows': 30})}


class AddCategoryForm(forms.ModelForm):
    """ form for adding new category """
    class Meta:
        model = Category
        fields = ['text']
        labels = {'text': ''}
