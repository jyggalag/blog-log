from django.db import models

class Category(models.Model):
    """ model post categoreis"""
    text = models.CharField(max_length=200)

    def __str__(self):
        return self.text


class BlogPost(models.Model):
    """ model for blog posts """
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    post = models.TextField()

    class Meta:
        verbose_name_plural = 'posts'

    def __str__(self):
        """ return string representation """
        return self.post[:50] + "..."


class Comment(models.Model):
    """ model for comments """
    post = models.ForeignKey(BlogPost)
    author = models.CharField(max_length=30)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'comments'

    def __str__(self):
        return self.text
